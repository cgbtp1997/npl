import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedServicesService {

  public pics: Array<any> = [];
  public isCustomizationMode = new BehaviorSubject(true);
  public uploadedPics: any = new BehaviorSubject(null);

  constructor() { }

  public setCustmizationMode(mode: boolean) {
    this.isCustomizationMode.next(mode);
  }

  public getCustmizationMode() {
    return this.isCustomizationMode;
  }

  public setUploadedPics(pics: Array<any>) {
    this.uploadedPics.next(pics);
  }

  public getUploadedPics() {
    return this.uploadedPics;
  }
}
