import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutsComponent } from './layouts.component';
import { NavbarModule } from './navbar/navbar.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdditionalNavBarComponent } from './additional-nav-bar/additional-nav-bar.component';
import { PhotopickerComponent } from './components/photopicker/photopicker.component';
import { ViewerComponent } from './components/viewer/viewer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorPickerModule } from '@iplab/ngx-color-picker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThemesComponent } from './components/themes/themes.component';
import { ImageCropperComponent } from 'src/app/image-cropper/component/image-cropper.component';
import { ImageModalComponent } from './components/viewer/image-modal/image-modal.component';
// import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxImageZoomModule } from 'ngx-image-zoom';

@NgModule({
  declarations: [LayoutsComponent, DashboardComponent, AdditionalNavBarComponent, PhotopickerComponent, ViewerComponent, ThemesComponent, ImageCropperComponent, ImageModalComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NavbarModule,
    SidebarModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
    TabsModule.forRoot(),
    ToastrModule.forRoot(),
    SharedModule,
    ModalModule.forRoot(),
    NgxImageZoomModule
  ]
})
export class LayoutModule { }
