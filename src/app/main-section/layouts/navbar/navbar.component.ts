import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { SharedServicesService } from 'src/app/shared-services/shared-services.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public isCustomizationMode: boolean = false;
  constructor(private sharedService: SharedServicesService) {
    sharedService.getCustmizationMode().subscribe(res => {
      this.isCustomizationMode = res;
    })
  }

  ngOnInit(): void {
    $(".filter").click(function () {
      $(".bride-groom-filter").toggleClass("height");
    })
  }

}
