import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarRoutingModule } from './navbar-routing.module';
import { NavbarComponent } from './navbar.component';
import { SharedServicesService } from 'src/app/shared-services/shared-services.service';


@NgModule({
  declarations: [NavbarComponent],
  imports: [
    CommonModule,
    NavbarRoutingModule
  ],
  providers: [SharedServicesService],
  exports: [NavbarComponent]
})
export class NavbarModule { }
