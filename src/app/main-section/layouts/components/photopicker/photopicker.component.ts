import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { SharedServicesService } from 'src/app/shared-services/shared-services.service';

@Component({
  selector: 'app-photopicker',
  templateUrl: './photopicker.component.html',
  styleUrls: ['./photopicker.component.css']
})
export class PhotopickerComponent implements OnInit {
  public pageType: string = 'two-sided'
  public pages: number = 0;
  public theme: number = 0;
  public albumImgsCount: number = 0;
  public images: Array<any> = [];
  public frames: Array<number> = [];
  public selectedImages: Array<any> = [];

  constructor(private router: Router, private route: ActivatedRoute, private sharedService: SharedServicesService) { }

  ngOnInit(): void {
    this.pages = this.route.snapshot.params.pages;
    this.theme = this.route.snapshot.params.theme;
    window.scrollTo({
      top:0
    })
    if(this.theme == 0 || this.theme ==1){
    this.albumImgsCount = this.pageType ? this.pages*2 : this.pages;
    }
    if(this.theme == 2 || this.theme ==2){
      this.albumImgsCount = this.pages;
      }
    this.getItemInFrames();    
   
  }

  public triggerFileUpload() {
    $("#file-upload").trigger('click');
  }

  public getItemInFrames() {
    for (let i = this.albumImgsCount; i >= 1; i--) {
      this.frames.push(i);
    }
  }

  public onFileUpload(event: any) {
    let tempArr: Array<any> = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {          
          tempArr.push(event.target.result);
          this.images.push(Object.assign({}, { src: event.target.result, id: this.images.length == 0 ? 2 : this.images[this.images.length - 1].id + 1, type: 'p', backupImg: event.target.result }))
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  public onPictureClick(image: any) {
    let index = this.selectedImages.findIndex(e => e.id == image.id);
    if (index > -1) {
      this.selectedImages.splice(index, 1);
      this.frames.push(this.frames[this.frames.length - 1]++);
    } else {
      if (this.selectedImages.length < this.albumImgsCount) {
        this.selectedImages.push(image);
        this.frames.pop();
      } else {
        alert(`Max ${this.albumImgsCount} photos can be added in this book`)
      }
     
    }
  }

  public selectedAllImages() {
    if (this.selectedImages.length != this.images.length) {
      this.deselectedAllImages();
      for (let i = 0; i < this.images.length; i++) {
        if (this.selectedImages.length < this.albumImgsCount) {
          this.selectedImages.push(Object.assign({}, this.images[i]));
        } else {
          // this.toastr.warning(`Max ${this.albumImgsCount} photos can be added in this book`);
          alert(`Max ${this.albumImgsCount} photos can be added in this book`)
          break;
        }
      }
      
      for (let i = 0; i < this.selectedImages.length; i++) {
        this.frames.pop();
      }
    } else {
      this.selectedImages = [...this.images];
    }

  }

  public deselectedAllImages() {
    this.selectedImages = [];
    this.frames = [];
    this.getItemInFrames();
  }

  public isSelectedImage(image: any) {
    let index = this.selectedImages.findIndex(e => e.id == image.id);
    return index > -1 ? true : false;
  }

  public goToViewer() {
    
    this.selectedImages.unshift(Object.assign({}, { src: "./assets/images/start_right_inner_cover.png", id: -1, type: 'p' }));
    this.selectedImages.push(Object.assign({}, { src: "./assets/images/last_left_inner_cover.png", id: -1, type: 'p' }));
    
    this.sharedService.setUploadedPics(this.selectedImages);
    
    this.router.navigate(['/viewer', this.pages, this.theme])
  }

}
