import { AfterViewChecked, AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewChecked {
  public data: Array<any> = [];
  public currIndex: number = -1;
  public detailModeOn: boolean = false;
  public images: Array<any> = [
    { src: 'https://i.imgur.com/weXVL8M.jpg', title: 'Image', desc: 'Description', active: true },
    { src: 'https://i.imgur.com/Rpxx6wU.jpg', title: 'Image', desc: 'Description', active: false },
    { src: 'https://i.imgur.com/83fandJ.jpg', title: 'Image', desc: 'Description', active: false },
    { src: 'https://i.imgur.com/JiQ9Ppv.jpg', title: 'Image', desc: 'Description', active: false }
  ]
  constructor(private router: Router, private _elementRef: ElementRef) {

  }
  ngAfterViewChecked(): void {

     
  }

  ngOnInit(): void {
  //   console.log($(".carousel"));
  //   $('#custCarousel').bind('slide.bs.carousel', function (e) {
  //     console.log('slide eventss!');
  // });
    // console.log(this._elementRef.nativeElement.querySelector('.ng-clear-wrapper'));
    // this._elementRef.nativeElement.querySelector('button').style.display = "none";
    this.data = [
      { name: 'Mohan', age: 21, height: '6 foot 2 inch', education: 'BA', father: 'Ram Pratap', mother: 'Tamila Bai', district: 'Indore' },
      { name: 'Sohan', age: 21, height: '6 foot 2 inch', education: 'BA', father: 'Ram Pratap', mother: 'Tamila Bai', district: 'Indore' },
      { name: 'Rohan', age: 21, height: '6 foot 2 inch', education: 'BA', father: 'Ram Pratap', mother: 'Tamila Bai', district: 'Indore' }
    ]

  }

  public addCandidate() {
    this.router.navigate(['/add-candidate']);
  }

  public showDetail(index: number) {
    this.detailModeOn = true;
    this.currIndex = index;
  }

  public editDetail() {
    this.router.navigate(['/add-candidate']);
  }


  public hideDetail() {
    this.detailModeOn = false;
  }

  public getActiveItem(index: number) {
    
  }

}
