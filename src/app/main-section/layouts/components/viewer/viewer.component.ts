import { AfterViewInit, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
// import * as $ from 'jquery';
import { FormArray } from '@angular/forms';
import { SharedServicesService } from 'src/app/shared-services/shared-services.service';
import { ActivatedRoute } from '@angular/router';
import { ImageModalComponent } from './image-modal/image-modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { fromEvent } from 'rxjs';
declare var $ :any;
import { map, debounceTime, flatMap, takeUntil } from "rxjs/operators";


@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.css']
})
export class ViewerComponent implements OnInit {
  public menuItems: Array<any> = ['photo', 'text', 'layout', 'background', 'border', 'style'];
  public pages: number = 0;
  public theme: number = 0;
  public images: Array<any> = [];
  public selectedImages: Array<any> = [];
  public frames: Array<any> = [];
  public picCountArray!: FormArray;
  public pageBundles: Array<any> = [];
  public zoom: number = 1;
  public zoomThreshold: number = 0.1;
  public currentMenu: String = "";
  public curr: number = 0;
  public prev: number = 0;
  public currentPage: any = '';
  public tempCurrentPage: any = '';
  public currImgIndex: number = -1;
  public textColor: string = "";
  public borderColor: string = "";
  public imageBorderWidth: number = 0;
  public text: string = "";
  public currentSide: string = "";
  public activePageId: number = -1;
  public textAreaSelected: boolean = false;
  public selectionType: string = '';
  public isToolbarOpen: boolean = false;
  public currTextIndex: number = -1;
  public currFrontLayoutClass: number = -1;
  public currBackLayoutClass: number = -1;
  public backgrounds: Array<string> = [
    'https://www.geeklawblog.com/wp-content/uploads/sites/528/2018/12/liprofile-656x369.png',
    'https://images.unsplash.com/photo-1516617442634-75371039cb3a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG8lMjBiYWNrZ3JvdW5kfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80'
  ];
  public borders: Array<string> = ['2px solid #DA6249', '4px solid #DABF49', '6px solid #B4DA49', '8px solid #4DDA49', '10px solid #49DACC', '12px solid #4963DA'];
  public styles: Array<string> = [
    'https://www.freecodecamp.org/news/content/images/2021/06/w-qjCHPZbeXCQ-unsplash.jpg',
    'https://i.pinimg.com/originals/af/8d/63/af8d63a477078732b79ff9d9fc60873f.jpg'
  ];
  public stylesForm: any = { 'font-family': '', 'font-size': 12, 'font-weight': 'unset', 'text-decoration': 'unset', 'font-style': 'unset', 'text-align': 'center', 'color': 'black', '-webkit-text-stroke': '', '-webkit-text-stroke-color': '', fontSize: 12 };

  modalRef: BsModalRef;
  constructor(private route: ActivatedRoute, private sharedService: SharedServicesService, private modalService: BsModalService) {
    this.pages = this.route.snapshot.params.pages;
    this.theme = this.route.snapshot.params.theme;
  }

  ngOnInit(): void {
  

    this.modalService.onHide.subscribe(result => {
      
      let pageTemp = this.currentPage ? this.currentPage : this.tempCurrentPage;
      if (pageTemp.charAt(1) == 'f') {
        if (this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
          this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.background.image = this.modalRef.content.croppedImage;
        }
        if (this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.bgTypeApplied == 'layout') {
          this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.picIds[this.currImgIndex].image = this.modalRef.content.croppedImage;
        }
      } else {
        if (this.pageBundles[+pageTemp.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
          this.pageBundles[+pageTemp.charAt(0) - 1].backSide.background.image = this.modalRef.content.croppedImage;
        }
        if (this.pageBundles[+pageTemp.charAt(0) - 1].backSide.bgTypeApplied == 'layout') {
          this.pageBundles[+pageTemp.charAt(0) - 1].backSide.picIds[this.currImgIndex].image = this.modalRef.content.croppedImage;
        }
      }
      // console.log('DATA', this.modalRef.content.croppedImage);
    });
    this.getAllImages();
    
    $("#wrapper .sidebar-nav li").on('click', function (e) {
      e.preventDefault();
      $("#wrapper2").addClass("active");
      $("#sidebar-close").addClass("active");
      $(".wrapper2-content-wrapper").css("display", "block");
    });

    $("#sidebar-close").on('click', function (e) {
      e.preventDefault();
      $("#wrapper2").removeClass("active");
      $("#sidebar-close").removeClass("active");
      $(".wrapper2-content-wrapper").css("display", "none");
    });
		var Page = (function () {

			var config = {
					$bookBlock: $('#bb-bookblock'),
					$navNext: $('#bb-nav-next'),
					$navPrev: $('#bb-nav-prev'),
					$navFirst: $('#bb-nav-first'),
					$navLast: $('#bb-nav-last')
				},
				init = function () {
					config.$bookBlock.bookblock({
						speed: 1000,
						shadowSides: 20000,
						shadowFlip: 1.7
					});
					initEvents();
				},
				initEvents = function () {

					var $slides = config.$bookBlock.children();
					// add navigation events
					config.$navNext.on('click touchstart', function () {
						config.$bookBlock.bookblock('next');
						return false;
					});
					config.$navPrev.on('click touchstart', function () {
						config.$bookBlock.bookblock('prev');
						return false;
					});

					config.$navFirst.on('click touchstart', function () {
						config.$bookBlock.bookblock('first');
						return false;
					});

					config.$navLast.on('click touchstart', function () {
						config.$bookBlock.bookblock('last');
						return false;
					});

					// add swipe events
					$slides.on({
						'swipeleft': function (event) {
              console.log("aaaa");
              
							config.$bookBlock.bookblock('next');
							return false;
						},
						'swiperight': function (event) {
							config.$bookBlock.bookblock('prev');
							return false;
						}
					});

					// add keyboard events
					$(document).keydown(function (e) {
						var keyCode = e.keyCode || e.which,
							arrow = {
								left: 37,
								up: 38,
								right: 39,
								down: 40
							};

						switch (keyCode) {
							case arrow.left:
								config.$bookBlock.bookblock('prev');
								break;
							case arrow.right:
								config.$bookBlock.bookblock('next');
								break;
						}
					});
				};

			return {
				init: init
			};

		})();
    Page.init();
  }

  public getAllImages() {
    
    this.sharedService.getUploadedPics().subscribe((res: any) => {
      if (res && res.length > 0) {
        this.selectedImages = res;
        
        localStorage.setItem('pics', res );
      } else {
        if (this.theme && this.theme == 0) {
          let a: any = localStorage.getItem('pics');
          
          this.selectedImages = a;
        } else if(this.theme && this.theme ==1) {
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/start_right_inner_cover.png", id: -1, type: 'p' }));
          for (let index = 0; index < this.pages * 2; index++) {
            this.selectedImages.push(Object.assign({}, { src: "./assets/images/placeholder.jpg", id: -1, type: 'p' }));
          }
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/last_left_inner_cover.png", id: -1, type: 'p' }));
        }
        else if(this.theme && this.theme == 2){
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/start_right_inner_cover.png", id: -1, type: 'p' }));
          for (let index = 0; index < this.pages; index++) {
            this.selectedImages.push(Object.assign({}, { src: "./assets/images/placeholder.jpg", id: -1, type: 'p' }));
          }
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/last_left_inner_cover.png", id: -1, type: 'p' }));
          
        }
        else{
          let a: any = localStorage.getItem('pics');
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/start_right_inner_cover.png", id: -1, type: 'p' }));
          for (let index = 0; index < this.pages; index++) {
            this.selectedImages.push(Object.assign({}, { src: "./assets/images/placeholder.jpg", id: -1, type: 'p' }));
          }
          this.selectedImages.push(Object.assign({}, { src: "./assets/images/last_left_inner_cover.png", id: -1, type: 'p' }));
        }

      }
      this.prepareAlbumWithImages();
    })
  }

  public getImagesForTheme1() {
    this.sharedService.getUploadedPics().subscribe((res: any) => {
      if (res && res.length > 0) {
        this.selectedImages = res;
        localStorage.setItem('pics', JSON.stringify({ pics: res }));
      } else {
        let a: any = localStorage.getItem('pics');
        this.selectedImages = a;
      }
      this.prepareAlbumWithImages();
    })
  }

  public prepareAlbumWithImages() {
    
    if(this.theme ==0 || this.theme ==1 ){
      for (let i = 0; i < this.pages; i++) {
        this.pageBundles.push({
          pageId: (i + 1).toString(), type: 'p',
          frontSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} },
          backSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} }
        });
      }
      // if ((this.selectedImages.length - 2) % 2 != 0) {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2) + 1
      // } else {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2 + 1);
      // }

      

      this.pageBundles[0].frontSide.layoutId = -1;
      this.pageBundles[this.pageBundles.length - 1].backSide.layoutId = -1;

      let startFrom: number = 0;
      let upto: number = 2;
      let tempArr: Array<any> = [];
      let pageNoFrom: number = 2;
      for (let i = 0; i < this.pageBundles.length; i++) {
        tempArr = this.selectedImages.slice(startFrom, upto);
        console.log(this.selectedImages);
        if (tempArr.length > 0 || i < this.pageBundles.length) {
          this.pageBundles[i].frontSide.pageNo = pageNoFrom + 1;
          this.pageBundles[i].backSide.pageNo = pageNoFrom;
          if (Math.floor((this.selectedImages.length - 2)) % 2 !== 0 && i == this.pageBundles.length - 1) {
            this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
          } else {
            if (tempArr[0]) {
              this.pageBundles[i].frontSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
              if (tempArr[1])
                this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[1].src}`, backupImg: tempArr[1].backupImg });
            }
          }
          startFrom = startFrom + 2;
          upto = upto + 2;
          pageNoFrom = pageNoFrom + 2;
        }
      }
      this.pageBundles[this.pageBundles.length - 1].backSide.picIds[0] = Object.assign({}, { image: this.selectedImages[this.selectedImages.length - 1].src, selected: false });
    }
    else{
      /*
      // if ((this.selectedImages.length - 2) % 2 != 0) {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2) + 1
      // } else {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2 + 1);
      // }

      for (let i = 0; i < this.pages; i++) {
        this.pageBundles.push({
          pageId: (i + 1).toString(), type: 'p',
          frontSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} },
          backSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} }
        });
      }

      this.pageBundles[0].frontSide.layoutId = -1;
      this.pageBundles[this.pageBundles.length - 1].backSide.layoutId = -1;

      let startFrom: number = 0;
      let upto: number = 2;
      let tempArr: Array<any> = [];
      let pageNoFrom: number = 2;
      for (let i = 0; i < this.pageBundles.length; i++) {
        tempArr = this.selectedImages.slice(startFrom, upto);
        
        if (tempArr.length > 0 || i < this.pageBundles.length) {
          this.pageBundles[i].frontSide.pageNo = pageNoFrom + 1;
          this.pageBundles[i].backSide.pageNo = pageNoFrom;
          if (Math.floor((this.selectedImages.length - 2)) % 2 !== 0 && i == this.pageBundles.length - 1) {
            this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
          } else {
            if (tempArr[0]) {
              this.pageBundles[i].frontSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
              if (tempArr[1])
                this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[1].src}`, backupImg: tempArr[1].backupImg });
            }
          }
          startFrom = startFrom + 2;
          upto = upto + 2;
          pageNoFrom = pageNoFrom + 2;
        }
      }
      this.pageBundles[this.pageBundles.length - 1].backSide.picIds[0] = Object.assign({}, { image: this.selectedImages[this.selectedImages.length - 1].src, selected: false });
      */
      for (let i = 0; i < this.pages; i++) {
        this.pageBundles.push({
          pageId: (i + 1).toString(), type: 'p',
          frontSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} },
          backSide: { layoutId: 0, layoutClass: '', noOfPics: [], picIds: [], textNodes: [], background: {}, bgTypeApplied: 'image', stylesSet: {} }
        });
      }
      // if ((this.selectedImages.length - 2) % 2 != 0) {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2) + 1
      // } else {
      //   this.pages = Math.ceil((this.selectedImages.length - 2) / 2 + 1);
      // }

      

      this.pageBundles[0].frontSide.layoutId = -1;
      this.pageBundles[this.pageBundles.length - 1].backSide.layoutId = -1;

      let startFrom: number = 0;
      let upto: number = 2;
      let tempArr: Array<any> = [];
      let pageNoFrom: number = 2;

      //console.log(this.selectedImages);

      let tempArr1 = this.selectedImages;
      let last = tempArr1[tempArr1.length - 1];
      let temp2 = [];
      temp2.push(tempArr1[0]);

      let proms = [];
      for(let i=1; i<tempArr1.length - 1; i++){
        
        proms.push(new Promise((res, rej) => {
          var canvas = document.createElement('canvas');
          var ctx    = canvas.getContext("2d");
          var parts = [];
          var img = new Image();
          //console.log(tempArr1[i], img);
          img.onload = function(){
            let w2 = img.width  / 2;  // 130
            let h2 = img.height;  // 40

            canvas.width  = w2;
            canvas.height = h2;

            for(var i=0; i<2; i++){

              var x = (-w2*i) % (w2*2),              // New X position
                  y = (h2*i)<=h2? 0 : -h2 ;          // New Y position
            
              ctx.drawImage(img, x, y, w2*2, h2); // imgObject, X, Y, width, height
              let data_url = canvas.toDataURL();
              temp2.push( {
                src: data_url,
                type: "p",
                backupImg: data_url
              } );      // ("image/jpeg") for jpeg
            
            }
            res(1);
          };
          img.src = tempArr1[i].src;
        }));
        
      }

      Promise.all(proms).then(x => {
        temp2.push(last);
        this.selectedImages = [...temp2];
        for (let i = 0; i < this.pageBundles.length; i++) {
          tempArr = this.selectedImages.slice(startFrom, upto);
          if (tempArr.length > 0 || i < this.pageBundles.length) {
            this.pageBundles[i].frontSide.pageNo = pageNoFrom + 1;
            this.pageBundles[i].backSide.pageNo = pageNoFrom;
            if (Math.floor((this.selectedImages.length - 2)) % 2 !== 0 && i == this.pageBundles.length - 1) {
              this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
            } else {
              if (tempArr[0]) {
                this.pageBundles[i].frontSide.background = Object.assign({}, { image: `${tempArr[0].src}`, backupImg: tempArr[0].backupImg });
                if (tempArr[1])
                  this.pageBundles[i].backSide.background = Object.assign({}, { image: `${tempArr[1].src}`, backupImg: tempArr[1].backupImg });
              }
            }
            startFrom = startFrom + 2;
            upto = upto + 2;
            pageNoFrom = pageNoFrom + 2;
          }
        }
        this.pageBundles[this.pageBundles.length - 1].backSide.picIds[0] = Object.assign({}, { image: this.selectedImages[this.selectedImages.length - 1].src, selected: false });
      });


      
    }
  }

  public toggleFlipBook(side: string) {

    $(".card").toggleClass('is-flipped');
    $('.page').removeClass('active');
    $('.page').removeClass('flipped');

    this.activePageId = side == 'startSide' ? -1 : this.pageBundles.length + 1;

    if (this.activePageId == -1) {
      $(".prevBtn").prop("disabled", true);
      $(".prevBtn").css("cursor", 'not-allowed');
      $(".card").removeClass("is-flipped");
    } else {
      $(".nextBtn").prop("disabled", true);
      $(".nextBtn").css("cursor", 'not-allowed');
      $(".card").removeClass("is-flipped");
      $('.page').addClass('flipped');
    }

  }

  public prevPage() {
    this.isToolbarOpen = false;
    this.text = ""
    this.currentPage = '';
    this.tempCurrentPage = '';

    this.resetImageTextSelection();

    this.activePageId--;

    if (!$(".card").hasClass("is-flipped")) {
      $(".card").addClass("is-flipped");
      $(".nextBtn").prop("disabled", false);
      $(".nextBtn").css("cursor", 'pointer');
    } else {
      $(".page.active:last").removeClass("active");
      $(".page.flipped:last").removeClass("flipped").addClass("active");
    }

    if (this.activePageId == -1) {
      $(".prevBtn").prop("disabled", true);
      $(".prevBtn").css("cursor", 'not-allowed');
      $(".card").removeClass("is-flipped");
    }

  }

  public nextPage() {

    this.isToolbarOpen = false;
    this.text = ""
    this.currentPage = '';
    this.tempCurrentPage = '';

    this.resetImageTextSelection();

    this.activePageId++;

    if (!$(".card").hasClass("is-flipped")) {
      $(".card").addClass("is-flipped");
      $(".prevBtn").prop("disabled", false);
      $(".prevBtn").css("cursor", 'pointer');
      $($(".page")[0]).addClass("active");
    } else {
      $(".page.active:last").removeClass("active");
      for (let index = 0; index < this.activePageId; index++) {
        $($(".page")[index]).addClass("flipped");
      }
      $($(".page")[this.activePageId]).addClass("active");
    }

    if (this.activePageId == this.pageBundles.length + 1) {
      $(".nextBtn").prop("disabled", true);
      $(".nextBtn").css("cursor", 'not-allowed');
      $(".card").removeClass("is-flipped");
    }

  }

  public getBackGroundUrl(bgTypeApplied: string, backgroundType: string, backgroundUrl: string) {
    if ((bgTypeApplied == 'image' && backgroundType == 'background') || (bgTypeApplied == 'image' && backgroundType == 'style') || (bgTypeApplied == 'background' && backgroundType == 'background') || (bgTypeApplied == 'style' && backgroundType == 'style') || (bgTypeApplied == 'layout' && backgroundType == 'background') || (bgTypeApplied == 'layout' && backgroundType == 'style') || ((bgTypeApplied == 'background' && backgroundType == 'layout') || (bgTypeApplied == 'layout' && backgroundType == 'layout'))) {
      return `url(${backgroundUrl}) round`;
    } else {
      return backgroundUrl;
    }
  }

  public collectLayoutId(evt: any, layoutClass: string, layoutId: number, imgCountOnLayout: number, textHolders: number, backgroundType: string, backgroundUrl: string) {
    debugger
    evt.stopPropagation();
    this.currentPage = this.currentPage ? this.currentPage : this.tempCurrentPage;
    let ind = this.pageBundles.findIndex(e => e.pageId == this.currentPage.charAt(0));

    if (ind > -1) {
      if (this.currentPage.charAt(1) == 'f') {
        backgroundUrl = this.getBackGroundUrl(this.pageBundles[ind].frontSide.bgTypeApplied, backgroundType, backgroundUrl);
        this.pageBundles[ind].frontSide.bgTypeApplied = backgroundType;

        this.currFrontLayoutClass = layoutId;
        this.pageBundles[ind].frontSide.layoutClass = layoutClass;
        this.pageBundles[ind].frontSide.layoutId = layoutId;
        this.pageBundles[ind].frontSide.noOfPics = this.generateArr(imgCountOnLayout, 'image', ind);
        console.log(this.pageBundles[ind].frontSide.textNodes);

        this.pageBundles[ind].frontSide.textNodes = this.generateArr(textHolders, 'text', ind);

        if (this.pageBundles[ind].frontSide.picIds.length == 0) {
          console.log('ffffffffffffff');
          for (let i = 1; i <= imgCountOnLayout; i++) {
            this.pageBundles[ind].frontSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }
          this.pageBundles[ind].frontSide.picIds[0] = Object.assign({}, { image: this.pageBundles[ind].frontSide.background.image, selected: false, backupImg: this.pageBundles[ind].frontSide.background.backupImg});
        } else if (imgCountOnLayout > this.pageBundles[ind].frontSide.picIds.length) {
          console.log('ggggggggggg');
          for (let i = this.pageBundles[ind].frontSide.picIds.length + 1; i <= imgCountOnLayout; i++) {
            this.pageBundles[ind].frontSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }
        } else if (imgCountOnLayout < this.pageBundles[ind].frontSide.picIds.length) {
          console.log('hhhhhhhhhhhh');
          
          let tempImgArr = this.pageBundles[ind].frontSide.picIds.filter((e: any) => e.image != './assets/images/placeholder.jpg');
          this.pageBundles[ind].frontSide.picIds = tempImgArr.splice(0, imgCountOnLayout);
          let remainingImgs = Math.abs(this.pageBundles[ind].frontSide.picIds.length - this.pageBundles[ind].frontSide.noOfPics.length);

          for (let i = 1; i <= remainingImgs; i++) {
            this.pageBundles[ind].frontSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }

        }

        switch (backgroundType) {
          case 'layout':
            if (this.pageBundles[ind].frontSide.bgTypeApplied != 'background' && this.pageBundles[ind].frontSide.bgTypeApplied != 'style') {
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            }
            this.pageBundles[ind].frontSide.bgTypeApplied = backgroundType;
            break;

          case 'background':

            if (this.pageBundles[ind].frontSide.bgTypeApplied != 'background') {
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            }
            console.log( this.pageBundles[ind].frontSide.background.image);
            
            this.pageBundles[ind].frontSide.bgTypeApplied = 'background';
            break;

          case 'style':
            if (this.pageBundles[ind].frontSide.bgTypeApplied != 'style') {
              this.pageBundles[ind].frontSide.picIds[0] = Object.assign({}, { image: this.pageBundles[ind].frontSide.background.image.slice(4, this.pageBundles[ind].frontSide.background.image.length - 7), selected: false });
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].frontSide.background.image = backgroundUrl;
            }
            this.pageBundles[ind].frontSide.bgTypeApplied = 'style';
        }
      } else {
        backgroundUrl = this.getBackGroundUrl(this.pageBundles[ind].backSide.bgTypeApplied, backgroundType, backgroundUrl);
        this.pageBundles[ind].backSide.bgTypeApplied = backgroundType;

        this.currBackLayoutClass = layoutId;
        this.pageBundles[ind].backSide.layoutClass = layoutClass;
        this.pageBundles[ind].backSide.layoutId = layoutId;
        this.pageBundles[ind].backSide.noOfPics = this.generateArr(imgCountOnLayout, 'image', ind);
        this.pageBundles[ind].backSide.textNodes = this.generateArr(textHolders, 'text', ind);

        if (this.pageBundles[ind].backSide.picIds.length == 0) {
          for (let i = 1; i <= imgCountOnLayout; i++) {
            this.pageBundles[ind].backSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }
          this.pageBundles[ind].backSide.picIds[0] = Object.assign({}, { image: this.pageBundles[ind].backSide.background.image, selected: false, backupImg: this.pageBundles[ind].backSide.background.backupImg});
        } else if (imgCountOnLayout > this.pageBundles[ind].backSide.picIds.length) {
          for (let i = this.pageBundles[ind].backSide.picIds.length + 1; i <= imgCountOnLayout; i++) {
            this.pageBundles[ind].backSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }
        } else if (imgCountOnLayout < this.pageBundles[ind].backSide.picIds.length) {
          let tempArr = this.pageBundles[ind].backSide.picIds.filter((e: any) => e.image != './assets/images/placeholder.jpg');
          this.pageBundles[ind].backSide.picIds = tempArr.splice(0, imgCountOnLayout);
          let remainingImgs = Math.abs(this.pageBundles[ind].backSide.picIds.length - this.pageBundles[ind].backSide.noOfPics.length);

          for (let i = 1; i <= remainingImgs; i++) {
            this.pageBundles[ind].backSide.picIds.push(Object.assign({}, {
              image: './assets/images/placeholder.jpg',
              selected: false
            }))
          }
        }

        switch (backgroundType) {
          case 'layout':
            if (this.pageBundles[ind].backSide.bgTypeApplied != 'background' && this.pageBundles[ind].backSide.bgTypeApplied != 'style') {
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            }
            this.pageBundles[ind].backSide.bgTypeApplied = backgroundType;
            break;

          case 'background':

            if (this.pageBundles[ind].backSide.bgTypeApplied != 'background') {
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            }
            this.pageBundles[ind].backSide.bgTypeApplied = 'background';
            break;

          case 'style':
            if (this.pageBundles[ind].backSide.bgTypeApplied != 'style') {
              this.pageBundles[ind].backSide.picIds[0] = Object.assign({}, { image: this.pageBundles[ind].backSide.background.image.slice(4, this.pageBundles[ind].backSide.background.image.length - 7), selected: false });
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            } else {
              this.pageBundles[ind].backSide.background.image = backgroundUrl;
            }
            this.pageBundles[ind].backSide.bgTypeApplied = 'style';
        }
      }
    }

  }

  public generateArr(digit: number, type: string, currentPageIndex: number) {
    let arr = [];
    switch (type) {
      case 'image':
        for (let i = 0; i < digit; i++) {
          arr.push(i + 1);
        }
        break;

      case 'text':
        let textHoldersWithTextInRecentLayout: Array<any> = this.pageBundles[currentPageIndex].frontSide.textNodes.filter((e: any) => e.text != 'Add text (optional)');

        if (digit == textHoldersWithTextInRecentLayout.length) {
          for (let i = 0; i < digit; i++) {
            arr.push(Object.assign({}, { text: textHoldersWithTextInRecentLayout[i].text, selected: false, display: 'block' }));
          }
          return arr;
        }

        if (digit < textHoldersWithTextInRecentLayout.length) {
          let count = 0;
          for (let i = 0; i < digit; i++) {
            arr.push(Object.assign({}, { text: textHoldersWithTextInRecentLayout[i].text, selected: false, display: 'block' }));
            count++;
            if (digit == count)
              break;
          }
          return arr;
        }

        if (digit > textHoldersWithTextInRecentLayout.length) {
          let count = 0;
          for (let i = 0; i < digit; i++) {
            arr.push(Object.assign({}, { text: textHoldersWithTextInRecentLayout[i] ? textHoldersWithTextInRecentLayout[i].text : 'Add text (optional)', selected: false, display: 'block' }));
            count++;
            if (digit == count)
              break;
          }
          return arr;
        }
        for (let i = 0; i < digit; i++) {
          arr.push(Object.assign({}, { text: 'Add text (optional)', selected: false, display: 'block' }));
        }
        return arr;
    }
    return arr;
  }

  public onPageSelection(evt: any, pageId: string, selectedItemType: string) {
    evt.stopPropagation()



    this.selectionType = selectedItemType;

    if (!(+pageId.charAt(0) == 1 && pageId.charAt(1) == 'f') && !(+pageId.charAt(0) == this.pageBundles.length && pageId.charAt(1) == 'b')) {
      this.currentPage = pageId;
      this.tempCurrentPage = pageId;
      this.currentSide = pageId.charAt(1);
      this.resetImageTextSelection();
    }

    this.openTextMenu(pageId.charAt(1))
  }

  public setImgOnLayout(image: any) {

    this.currentPage = '';
    let pageTemp = this.currentPage ? this.currentPage : this.tempCurrentPage;
    let page = this.pageBundles.find(e => e.pageId == pageTemp.charAt(0));

    if (page) {
      if (pageTemp.charAt(1) == 'f') {
        if (page.frontSide.layoutId) {
          page.frontSide.picIds[this.currImgIndex] = Object.assign({}, { image: image.src, selected: true, backupImg: image.backupImg});
        } else {
          page.frontSide.background.image = `${image.src}`;
        }
      } else {
        if (page.backSide.layoutId) {
          page.backSide.picIds[this.currImgIndex] = Object.assign({}, { image: image.src, selected: true, backupImg: image.backupImg});
        } else {
          page.backSide.background.image = `${image.src}`;
        }
      }
    }

  }

  public setBgOnLayout(evt: any, background: string) {
    // evt.stopPropagation();
    // this.currentPage = '';
    let pageTemp = this.currentPage ? this.currentPage : this.tempCurrentPage;

    let page = this.pageBundles.find(e => e.pageId == pageTemp.charAt(0));

    if (page) {
      if (pageTemp.charAt(1) == 'f') {
        if (this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
          alert("Can't set background when Image is in full mode !");
          return
        }

        page.frontSide.layoutClass = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.layoutClass ? this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.layoutClass : 'single_img_1 type_1_Col';
        page.frontSide.currFrontLayoutClass = page.frontSide.layoutClass;
        page.frontSide.layoutId = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.layoutId ? this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.layoutId : 11;
        page.frontSide.noOfPics = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.noOfPics.length > 0 ? this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.noOfPics : [1];
        page.frontSide.textHolders = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.textNodes.length;
        this.collectLayoutId(evt, page.frontSide.layoutClass, page.frontSide.layoutId, page.frontSide.noOfPics.length, page.frontSide.textHolders, 'background', background);
      } else {
        if (this.pageBundles[+pageTemp.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
          alert("Can't set background when Image is in full mode !");
          return
        }

        page.backSide.layoutClass = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.layoutClass ? this.pageBundles[+pageTemp.charAt(0) - 1].backSide.layoutClass : 'single_img_1 type_1_Col';
        page.backSide.currBackLayoutClass = page.backSide.layoutClass;
        page.backSide.layoutId = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.layoutId ? this.pageBundles[+pageTemp.charAt(0) - 1].backSide.layoutId : 11;
        page.backSide.noOfPics = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.noOfPics.length > 0 ? this.pageBundles[+pageTemp.charAt(0) - 1].backSide.noOfPics : [1];
        page.backSide.textHolders = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.textNodes.length;
        this.collectLayoutId(evt, page.backSide.layoutClass, page.backSide.layoutId, page.backSide.noOfPics.length, page.backSide.textHolders, 'background', background);
      }
    }

    // if (page) {
    //   if (pageTemp.charAt(1) == 'f') {
    //     page.frontSide.bgTypeApplied = true;
    //     page.frontSide.layoutClass = 'single_img_1 type_1_Col';
    //     page.frontSide.currFrontLayoutClass = 'single_img_1 type_1_Col';
    //     page.frontSide.layoutId = 11;
    //     page.frontSide.noOfPics = 1;
    //     page.frontSide.background.image = `url(${background}) round`;
    //     if (page.frontSide.bgTypeApplied)
    //       page.frontSide.picIds[0] = Object.assign({}, { image: page.frontSide.background.image.slice(4, page.frontSide.background.image.length - 7), selected: false });

    //   } else {
    //     page.backSide.bgTypeApplied = true;
    //     if (page.backSide.bgTypeApplied)
    //       page.backSide.picIds[0] = Object.assign({}, { image: page.backSide.background.image.slice(4, page.backSide.background.image.length - 7), selected: false });
    //     page.backSide.background.image = `url(${background}) round`;
    //   }
    // }

  }

  public onLayoutImageSelection(evt: any, pageIndex: number, imageIndex: number, pageSide: string) {

    evt.stopPropagation();
    this.currentPage = this.pageBundles[pageIndex].pageId + pageSide;
    this.tempCurrentPage = this.pageBundles[pageIndex].pageId + pageSide;
    this.currImgIndex = imageIndex;
    this.isToolbarOpen = true;
    this.selectionType = 'image';

    this.resetImageTextSelection();

    if (pageSide == 'f')
      this.pageBundles[pageIndex].frontSide.picIds[imageIndex].selected = true;
    else
      this.pageBundles[pageIndex].backSide.picIds[imageIndex].selected = true;

  }

  public openFontDropdown() {

    $('.js-link').on('click', function (e) {
      e.preventDefault();

      $('.js-dropdown-list').slideToggle(200);
    });

  }

  public chooseFont() {

    $('.js-dropdown-list').find('li').on('click', function () {
      var text = $(this).html();
      var icon = '<i class="fa fa-chevron-down"></i>';
      $('.js-link').html(text + icon);
      $('.js-dropdown-list').slideToggle(200);
      if (text === '* Reset') {
        $('.js-link').html('Select one option' + icon);
      }
    });


    this.stylesForm['font-family'] = $('.js-link').text();
  }

  public chooseColor() {

    $("#color-picker-one").on('click', function () {
      $(".chrome-picker-one").toggleClass("d-none");
      $(".color-shutter").toggleClass("d-none");
    });

  }

  public closeColorPicker() {
    $(".chrome-picker-one").toggleClass("d-none");
    $(".chrome-picker-two").toggleClass("d-none");
    $(".color-shutter").toggleClass("d-none");

  }

  public chooseBorderColor() {

    $("#color-picker-two").on('click', function () {
      $(".chrome-picker-two").toggleClass("d-none")
      $(".color-shutter").toggleClass("d-none");
    })

  }

  public onTextSelect(evt: any, pageIndex: number, side: string, textIndex: number) {

    evt.stopPropagation();
    this.selectionType = 'text';
    this.isToolbarOpen = true;
    this.currentPage = this.pageBundles[pageIndex].pageId + side;
    this.currentSide = side;
    this.currTextIndex = textIndex;
    this.currImgIndex = -1;

    this.resetImageTextSelection();

    if (side == 'f') {
      this.pageBundles[pageIndex].frontSide.textNodes.map((e: any, i: number) => {
        if (i == textIndex) {
          e.selected = true
          this.text = this.pageBundles[pageIndex].frontSide.textNodes[textIndex].text;
        }
      });
    } else {
      this.pageBundles[pageIndex].backSide.textNodes.map((e: any, i: number) => {
        if (i == textIndex) {
          e.selected = true
          this.text = this.pageBundles[pageIndex].backSide.textNodes[textIndex].text;
        }
      });
    }

    $(".custom-text-editor").css('visibility', 'unset');

  }

  public onTextChange() {

    let ind = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));
    if (ind > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[ind].frontSide.textNodes[this.currTextIndex].text = this.text ? this.text : 'Add text (optional)';
      } else {
        this.pageBundles[ind].backSide.textNodes[this.currTextIndex].text = this.text ? this.text : 'Add text (optional)';
      }
    }

  }

  public setImgBorder(value: any, attribute: string) {
    if (!this.currentPage) {
      alert("Please select a page.");
      return
    }
    let color = '';
    switch (attribute) {
      case 'color':
        if (this.currentPage.charAt(1) == 'f') {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.picIds[this.currImgIndex].imgBorder = `${this.imageBorderWidth}px solid ${value}`
        } else {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.picIds[this.currImgIndex].imgBorder = `${this.imageBorderWidth}px solid ${value}`
        }
        break;

      case 'width':
        if (this.currentPage.charAt(1) == 'f') {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          color = this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.picIds[this.currImgIndex].imgBorder.split("solid")[1];
          this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.picIds[this.currImgIndex].imgBorder = `${value}px solid ${color}`
        } else {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          color = this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.picIds[this.currImgIndex].imgBorder.split("solid")[1];
          this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.picIds[this.currImgIndex].imgBorder = `${value}px solid ${color}`
        }
        break;

      case 'border_predefined_attr':
        if (this.currentPage.charAt(1) == 'f') {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          this.pageBundles[+this.currentPage.charAt(0) - 1].frontSide.picIds[this.currImgIndex].imgBorder = value;
        } else {
          if (this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
            alert("Can't set border styles when Image is in full mode !");
            return
          }
          if (this.currImgIndex < 0) {
            alert("Select an image to apply border styles.");
            return
          }
          this.pageBundles[+this.currentPage.charAt(0) - 1].backSide.picIds[this.currImgIndex].imgBorder = value;
        }
        break;
    }
  }

  public setStyle(property: string, value?: string) {

    this.stylesForm['color'] = this.textColor;

    switch (property) {
      case 'size':
        this.stylesForm['font-size'] = this.stylesForm.fontSize + 'px';
        break;
      case 'bold':
        this.stylesForm['font-weight'] = this.stylesForm['font-weight'] === 'unset' ? 'bold' : 'unset';
        break;
      case 'italic':
        this.stylesForm['font-style'] = this.stylesForm['font-style'] === 'unset' ? 'italic' : 'unset';
        break;
      case 'underline':
        this.stylesForm['text-decoration'] = this.stylesForm['text-decoration'] === 'unset' ? 'underline' : 'unset';
        break;
      case 'text-align':
        this.stylesForm['text-align'] = value;
        break;
    }

    let ind = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));

    if (ind > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[ind].frontSide.stylesSet = this.stylesForm;
      } else {
        this.pageBundles[ind].backSide.stylesSet = this.stylesForm;
      }
    }

  }

  public openTextMenu(currentSide: string) {

    // $(".wrapper2-content-wrapper").css("display", "block");
    // $("#wrapper2").addClass("active");
    // $("#sidebar-close").addClass("active");
    // this.currentMenu = 'Text';
    let index = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));

    if (index > -1) {
      if (this.selectionType == 'background') {
        if (currentSide == 'f' && this.pageBundles[index].frontSide.background.image !== 'linear-gradient(to right bottom, rgb(255, 255, 255), rgb(204, 204, 204))') {
          this.isToolbarOpen = true;
        }
        if (currentSide == 'b' && this.pageBundles[index].backSide.background.image !== 'linear-gradient(to right bottom, rgb(255, 255, 255), rgb(204, 204, 204))') {

          this.isToolbarOpen = true;
        }
      }
    }

  }

  public hideItem(evt: any) {

    evt.stopPropagation();
    let index = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));

    switch (this.selectionType) {
      case 'background':
        this.removeBackgroundImage(index);
        break;

      case 'image':
        this.removeImage(index);
        break

      case 'text':
        this.removeText(index);
        break
    }

    this.isToolbarOpen = false;

  }

  public removeBackgroundImage(index: number) {

    if (index > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[index].frontSide.background.image = (this.pageBundles[index].frontSide.background.image !== 'url("./assets/images/placeholder.jpg")') ? 'url("./assets/images/placeholder.jpg")' : 'linear-gradient(to right bottom, rgb(255, 255, 255), rgb(204, 204, 204))';
      } else {
        this.pageBundles[index].backSide.background.image = (this.pageBundles[index].backSide.background.image !== 'url("./assets/images/placeholder.jpg")') ? 'url("./assets/images/placeholder.jpg")' : 'linear-gradient(to right bottom, rgb(255, 255, 255), rgb(204, 204, 204))';
      }
    }

  }

  public removeImage(index: number) {

    if (index > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[index].frontSide.picIds[this.currImgIndex].image = './assets/images/placeholder.jpg';
      } else {
        this.pageBundles[index].backSide.picIds[this.currImgIndex].image = './assets/images/placeholder.jpg';
      }
    }

  }

  public removeText(index: number) {

    if (index > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[index].frontSide.textNodes[this.currTextIndex].display = 'none';
        this.pageBundles[index].frontSide.stylesSet = {};
      } else {
        this.pageBundles[index].backSide.textNodes[this.currTextIndex].display = 'none';
        this.pageBundles[index].backSide.stylesSet = {};
      }
    }

  }

  public showTextArea() {

    let ind = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));

    if (ind > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[ind].frontSide.textNodes[this.currTextIndex].display = 'block';
        this.pageBundles[ind].frontSide.textNodes[this.currTextIndex].text = 'Add text (optional)';
      } else {
        this.pageBundles[ind].backSide.textNodes[this.currTextIndex].display = 'block';
        this.pageBundles[ind].backSide.textNodes[this.currTextIndex].text = 'Add text (optional)';
      }
    }

  }

  public goToPage(pageIndex: number) {

    if (pageIndex == -1) {
      this.activePageId = 0;
    }

    if (pageIndex == -2) {
      this.activePageId = 4;
      $(".card").removeClass('is-flipped');
    }

    if (this.activePageId >= -1 || this.activePageId <= 4) {
      $(".nextBtn").prop("disabled", false);
      $(".nextBtn").css("cursor", 'pointer');
      $(".prevBtn").prop("disabled", false);
      $(".prevBtn").css("cursor", 'pointer');
    }

    $(".card").addClass('is-flipped');

    for (let i = 0; i < $(".book .page").length; i++) {
      $($(".book .page")[i]).removeClass('flipped');
      $($(".book .page")[i]).removeClass('active');
    }

    if (pageIndex === -1) {
      for (let i = 0; i < pageIndex; i++) {
        $($(".book .page")[i]).removeClass('flipped');
      }
      $($(".book .page")[0]).addClass('active');
    } else {
      for (let i = 0; i < pageIndex; i++) {
        $($(".book .page")[i]).addClass('flipped');
      }
      $($(".book .page")[pageIndex]).addClass('flipped');
      $($(".book .page")[pageIndex + 1]).addClass('active');
    }

    this.tempCurrentPage = '';
    this.currentPage = '';

    if (pageIndex >= 0) {
      this.activePageId = this.pageBundles[pageIndex].pageId;
      this.resetImageTextSelection();
    }

    this.isToolbarOpen = false;
    this.text = ""

  }

  public setImgAsBackground(evt: any) {

    evt.stopPropagation();
    this.selectionType = 'background';
    let ind = this.pageBundles.findIndex(e => e.pageId == (this.currentPage ? this.currentPage.charAt(0) : this.tempCurrentPage.charAt(0)));

    if (ind > -1) {
      if (this.currentSide == 'f') {
        this.pageBundles[ind].frontSide.background.image = `${this.pageBundles[ind].frontSide.picIds[this.currImgIndex].image}`;
        this.pageBundles[ind].frontSide.bgTypeApplied = 'image';
        this.pageBundles[ind].frontSide.picIds = [];
        this.pageBundles[ind].frontSide.noOfPics = [];
        this.pageBundles[ind].frontSide.layoutId = 0;
      } else {
        this.pageBundles[ind].backSide.background.image = `${this.pageBundles[ind].backSide.picIds[this.currImgIndex].image}`;
        this.pageBundles[ind].backSide.bgTypeApplied = 'image';
        this.pageBundles[ind].backSide.picIds = [];
        this.pageBundles[ind].backSide.noOfPics = [];
        this.pageBundles[ind].backSide.layoutId = 0;
      }
    }
  }

  public resetImageTextSelection() {
    for (let i = 0; i < this.pageBundles.length; i++) {
      this.pageBundles[i].frontSide.picIds.map((e: any) => e.selected = false);
      this.pageBundles[i].backSide.picIds.map((e: any) => e.selected = false);
      this.pageBundles[i].frontSide.textNodes.map((e: any) => e.selected = false);
      this.pageBundles[i].backSide.textNodes.map((e: any) => e.selected = false);
    }
  }

  public openImageCropperModal(evt: any) {
    let imageToTransform = '';
    let pageTemp = this.currentPage ? this.currentPage : this.tempCurrentPage;
    if (pageTemp.charAt(1) == 'f') {
      if (this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.bgTypeApplied == 'image') {
        console.log(imageToTransform = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.background.backupImg);
        
        imageToTransform = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.background.backupImg;
      }
      if (this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.bgTypeApplied == 'layout') {
        imageToTransform = this.pageBundles[+pageTemp.charAt(0) - 1].frontSide.picIds[this.currImgIndex].backupImg;
      }
    } else {
      if (this.pageBundles[+pageTemp.charAt(0) - 1].backSide.bgTypeApplied == 'image') {
        imageToTransform = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.background.backupImg;
      }
      if (this.pageBundles[+pageTemp.charAt(0) - 1].backSide.bgTypeApplied == 'layout') {
        imageToTransform = this.pageBundles[+pageTemp.charAt(0) - 1].backSide.picIds[this.currImgIndex].backupImg;
      }
    }
    // console.log(imageToTransform.slice(0, 100));

    this.modalRef = this.modalService.show(ImageModalComponent, {
      initialState: {
        data: imageToTransform
      }
    });
  }

  public zoomOut() {
    if(this.zoom < 2) {
      this.zoom += this.zoomThreshold;
    }
  }

  public zoomIn() {
    if(this.zoom > 1) {
      this.zoom -= this.zoomThreshold;
    }
  }

  // public triggerFileUpload() {
  //   $("#file-upload").trigger('click');
  // }

  // public onFileUpload(event: any) {
  //   console.log(event.target.files);
  //   let tempArr: Array<any> = [];
  //   if (event.target.files && event.target.files[0]) {
  //     var filesAmount = event.target.files.length;
  //     for (let i = 0; i < filesAmount; i++) {
  //       var reader = new FileReader();

  //       reader.onload = (event: any) => {
  //         tempArr.push(event.target.result);
  //         this.images.push(Object.assign({}, { src: event.target.result, id: this.images.length == 0 ? 2 : this.images[this.images.length - 1].id + 1, type: 'p' }))
  //         console.log(this.images);
  //       }
  //       reader.readAsDataURL(event.target.files[i]);
  //     }

  //     let a: any = localStorage.getItem('pics');
  //     this.selectedImages = JSON.parse(a).pics;
  //     this.selectedImages.splice(this.selectedImages.length - 1, 0, ...this.images);
  //     this.sharedService.setUploadedPics(this.selectedImages);
  //     this.getImagesForTheme1();
  //   }
  // }
}
// 2=double,3=triple, 4 = quadruple, 5= quintuple, 6= sextuple, 7 = septuple 8 = octuple