import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.css']
})
export class ThemesComponent implements OnInit {
  public themeCategory: Array<any> = [
    { name: 'all', label: 'All', active: true }, { name: 'two-sided', label: 'Two Sided', active: false }, { name: 'panoramic', label: 'Panoramic', active: false }
  ];

  public categoryList: Array<string> = ['Full page photos (Two Sided)', 'Blank Theme (Two Sided)', 'Full page photos (Panoramic)', 'Blank Theme (Panoramic)'];
  public selectedTheme: string = 'all';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public selectTheme(index: number) {
    this.themeCategory.map((ele, i, arr) => ele.active = false);
    this.themeCategory[index].active = true;
    this.selectedTheme = this.themeCategory[index].name;
  }

  public goToPhotopicker(categoryName: string) {
    switch (categoryName) {
      case 'Full page photos (Two Sided)':
        this.router.navigate(['/photopicker', 5, 0,])
        break;

      case 'Blank Theme (Two Sided)':
        this.router.navigate(['/viewer', 5, 1])
        break;

      case 'Full page photos (Panoramic)':
        this.router.navigate(['/photopicker', 5, 2])
        break;

      case 'Blank Theme (Panoramic)':
        this.router.navigate(['/viewer', 5, 3])
        break;
    }
  }

}
