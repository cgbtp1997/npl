import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { SharedServicesService } from 'src/app/shared-services/shared-services.service';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.css']
})
export class LayoutsComponent implements OnInit {
  public isCustomizationMode: boolean = false;
  
  constructor(private sharedService: SharedServicesService) {
    sharedService.getCustmizationMode().subscribe(res => {
      this.isCustomizationMode = res;
    })
  }

  ngOnInit(): void {
    // jQuery(function ($) {
    // $("#show-sidebar").click(function (this) {
    //   $(".back-drop-section").addClass("back-drop");
    // })
    // $("#close-sidebar").click(function () {
    //   $(".back-drop-section").removeClass("back-drop");
    // })

    // $(".main-page").click(function () {
    //   $(".back-drop-section").removeClass("back-drop");
    //   $(".page-wrapper").removeClass("toggled");
    // })

    $(".sidebar-dropdown > a").click(function () {
      $(".sidebar-submenu").slideUp(200);
      if (
        $(this)
          .parent()
          .hasClass("active")
      ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .parent()
          .removeClass("active");
      } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .next(".sidebar-submenu")
          .slideDown(200);
        $(this)
          .parent()
          .addClass("active");
      }
    });

    $("#close-sidebar").click(function () {
      $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(function () {
      $(".page-wrapper").addClass("toggled");
    });
  }

}
