import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PhotopickerComponent } from './components/photopicker/photopicker.component';
import { ThemesComponent } from './components/themes/themes.component';
import { ViewerComponent } from './components/viewer/viewer.component';
import { LayoutsComponent } from './layouts.component';


const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    path: '',
    component: LayoutsComponent,
    children: [
      { path: '', redirectTo: '/themes', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'themes', component: ThemesComponent },
      { path: 'photopicker/:pages/:theme', component: PhotopickerComponent },
      { path: 'viewer/:pages/:theme', component: ViewerComponent },
      // { path: 'dashboard/detail', component: BridegroomDetailComponent }  ,
      // { path: 'add-candidate', component: AddCandidateComponent },
      // { path: 'relatives', component: RelativesComponent },
      // { path: 'relatives/add-relative', component: AddRelativesComponent },
      // { path: 'central-persons', component: CentralPersonsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
