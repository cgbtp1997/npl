import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public ages: Array<any> = [];
  public heights: Array<any> = [];
  public thakurType: Array<any> = [];
  public graduations: Array<any> = [];
  public postGraduations: Array<any> = [];
  public varVadhuStatus: Array<any> = [];
  public varSkinTones: Array<any> = [];
  public myage: number = 18;
  public selectedSkinTone: number = -1;

  public signupForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    console.log(this.signupForm);
  }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      name: ['', Validators.required],
      gender: ['', Validators.required],
      spouse: ['', Validators.required],
      thakurType: ['', Validators.required],
      dob: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      village: ['', Validators.required],
      password: ['', Validators.required],
      confirmPwd: ['', Validators.required]
    });
  }

  get f() { return this.signupForm.controls; }

  public calculateAge(evt: any) {
    let dob = new Date(evt.target.value)
    var ageDifMs = Date.now() - new Date(dob).getTime();
    var ageDate = new Date(ageDifMs);
    this.myage = dob ? Math.abs(ageDate.getUTCFullYear() - 1970) : 0;
    console.log(this.myage);
  }

}
