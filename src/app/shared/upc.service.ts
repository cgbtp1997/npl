import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UpcService {

  constructor() { }

  public getupc(val: string) {
    return val.toUpperCase();
  }
}
